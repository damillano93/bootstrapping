#include <cdefs.h>
POUND @(#) $RCSfile: Smake.cpp,v $: $Revision: #2 $, $Date: 2003/03/07 $
POUND @(#) Copyright (c), 2001 Insightful, Inc.  All rights reserved.

DEST=..

QFILES=  readdata.q

all : $(QFILES)
install : $(DEST)/$(DATA_DIR) $(DEST)/$(DATA_DIR)/$(HELP_DIR) install.funs

install.funs : $(QFILES)
	QINSTALL $(DEST)/$(DATA_DIR) $(QFILES)

readdata.q: readdata.ssc
	cp readdata.ssc readdata.q
