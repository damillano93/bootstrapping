This directory contains data sets for chapter 18 of
	The Practice of Business Statistics
by Moore, McCabe, Duckworth, and Sclove.
The chapter is titled
	Bootstrap Methods and Permutation Tests
and was written by Hesterberg, Monaghan, Moore, Clipson, and Epstein.

The naming conventions are:
	ex18_001.dat for Exercise 18.1
	eg18_001.dat for Example 18.1
	ta18_001.dat for Table 18.1
	ca18_001.dat for Case 18.1
Some datasets are also named individually.
The same data may appear in multiple files.


##################################################
The file
	readdata.ssc
contains code to read the data into S-PLUS.
There is a free student version of S-PLUS, available at
	elms03.e-academy.com/splus
In addition, a student S-PLUS library
containing the data sets already loaded into S-PLUS,
menu-driven access to capabilities you'll need,
and a manual that accompanies this chapter,
will be available at one or both of
	www.insightful.com/Hesterberg/bootstrap
	www.whfreeman.com
You may also order an S-PLUS manual as a
supplement to this book from
{\tt www.whfreeman.com}; the
authors are Snow and Chihara.


################## Case Studies ##################
verizon.dat	Verizon data (both ILEC and CLEC)
ca18_001.dat	same as verizon.dat

ca18_002.dat	Seattle Real Estate Prices, 2002 & 2001
ta18_001.dat	2002 Seattle data
ta18_005.dat	Seattle 2001
ex18_073.dat	Seattle 2000

ca18_003.dat	Baseball salaries
ta18_002.dat	Baseball salaries, same as ca18_003.dat

################## Additional Tables ##################
ta18_003.dat	Weight and gas mileage
ta18_004.dat	Directed reading activities (DRP)

################## Examples ##################
eg18_001.dat	same as ilec.dat
eg18_002.dat	same as ilec.dat
eg18_003.dat	same as ilec.dat
eg18_004.dat	same as Seattle2002.dat
eg18_005.dat	same as Seattle2002.dat
eg18_006.dat	same as Seattle2002.dat
eg18_007.dat	same as clec.dat
eg18_008.dat	New Jersey Lottery
eg18_009.dat	same as Seattle2002.dat
eg18_010.dat	same as ca18_003.dat, Baseball salaries
eg18_011.dat	same as Seattle2002.dat
eg18_012.dat	same as ta18_004.dat, DRP data
eg18_013.dat	same as ta18_004.dat, DRP data
eg18_014.dat	same as verizon.dat
eg18_015.dat	French Language instruction; from ta07_002.dat

################## Exercises ##################
ex18_001.dat	SRS size 6 from: ILEC
ex18_002.dat	White female hourly workers, subset of ta01_008.dat
ex18_003.dat	Spending by shoppers
ex18_004.dat	Guinea Pig survival times
ex18_005.dat	SRS size 10 from: Spending by shoppers, 
ex18_006.dat	SRS size 20 from: Guinea pig survival times
  18_007	(see exercise 18.3; Spending by shoppers)
  18_008	(see exercise 18.3; Spending by shoppers)
  18_009	(see exercise 18.4; Guinea Pig)
  18_010	(see exercise 18.3; Spending by shoppers)
  18_011	(see exercise 18.3; Spending by shoppers)
  18_012	(see exercise 18.3; Spending by shoppers)
ex18_013.dat	same as verizon.dat
ex18_014.dat	same as ta18_004.dat, DRP data
ex18_015.dat	Healthy vs Failed companies; from ta07_004.dat
  18_016	(no data)
ex18_017.dat	same as Seattle2002.dat
ex18_018.dat	same as Seattle2002.dat
ex18_019.dat	Really Normal Data	
ex18_020.dat	CEO salaries
ex18_021.dat	Clothing for runners
  18_022	(see previous)
ex18_023.dat	Mortgage refusal rates
ex18_024.dat	Billionaires
ex18_025.dat	same as clec.dat
  18_026	(no data)
ex18_027.dat	same as ilec.dat
  18_028	(see previous exercises)
  18_029	(no data)
ex18_030.dat	IQ scores of seventh-grade students
ex18_031.dat	same as ca18_003.dat, Baseball salaries
ex18_032.dat	Wages and length of service; from ta10_001.dat
ex18_033.dat	same as Seattle2002.dat
ex18_034.dat	same as Seattle2002.dat
  18_035	(see exercise 18.32)
ex18_036.dat	same as clec.dat
  18_037	(see example 18.7, Verizon)
  18_038	(see exercise 18.19)
  18_039	(see exercise 18.21)
ex18_040.dat	Earnings of black male bank workers; from ta01_008.dat
ex18_041.dat	Bootstrap to check traditional inference
  18_042	(see previous exercise)
ex18_043.dat	Iowa housing prices; from ta02_013.dat
ex18_044.dat	same as ex18_043.dat
ex18_045.dat	same as ta18_003.dat
ex18_046.dat	same as ca18_003.dat, Baseball salaries
ex18_047.dat	Black female hourly workers, subset of ta01_008.dat
  18_048	(by hand)
ex18_049.dat	same as Seattle.dat
ex18_050.dat	same as ex18_015.dat
  18_051	(no data)
  18_052	(no data)
  18_053	(two by two table)
ex18_054.dat	Matched pairs: designing controls; from ex07_040.dat
ex18_055.dat	same as ca18_003.dat, Baseball salaries
ex18_056.dat	Female basketball players
ex18_057.dat	Reaction time; from ta02_012.dat	
ex18_058.dat	Fast Food	
  18_059	(two by two table)
ex18_060.dat	same as Seattle.dat
ex18_061.dat	same as ex18_043.dat
  18_062	(two by two table)
  18_063	(see previous)
ex18_064.dat	Another Verizon data set
  18_065	(see previous)
ex18_066.dat	same as ex18_015.dat
ex18_067.dat	Executives learn Spanish; from ex07_042.dat
ex18_068.dat	Piano lessons
  18_069	(simulation)
ex18_070.dat	Age of personal trainers
ex18_071.dat	Stock Returns; from ta02_006.dat
ex18_072.dat	Blockbuster stock
ex18_073.dat	Seattle 2000 Real Estate
ex18_074.dat	Radon Detectors
  18_075	(see previous)	
ex18_076.dat	Do nurses use gloves?
  18_077	(see previous)	
ex18_078.dat	Changes in urban unemployment
  18_079	(two by two table)	Ice cream preferences
ex18_080.dat	Word counts in magazine ads
  18_081	(see previous)
ex18_082.dat	Hyde Park burglaries
  18_083	(see previous)
